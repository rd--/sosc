(import (rnrs) (mk-r6rs core))

; [string]
(define sosc-file-seq
  (read-file-lines "sosc-file-seq.text"))

(define sosc-file-seq-sys
  (lambda (c)
    (list
     (string-append "udp." c ".scm")
     (string-append "tcp." c ".scm")
     )))

(define at-src
  (lambda (x)
    (string-append "../src/" x)))

(define sosc-src
  (lambda (c)
    (map at-src (append sosc-file-seq (sosc-file-seq-sys c)))))

(define lib-dir (list-ref (command-line) 1))

; ikarus
(mk-r6rs '(sosc core)
         (sosc-src "ikarus")
         (string-append lib-dir "/r6rs/sosc/core.ikarus.sls")
         '((rnrs) (rhs core) (prefix (ikarus) ikarus:))
         '()
         '())

; chezscheme
;(mk-r6rs '(sosc core)
;         (sosc-src "chezscheme")
;         (string-append lib-dir "/r6rs/sosc/core.chezscheme.sls")
;         '((rnrs) (rhs core) (prefix (chezscheme) chezscheme:))
;         '()
;         '())

; guile
;(mk-r6rs '(sosc core)
;         (sosc-src "guile")
;         (string-append lib-dir "/r6rs/sosc/core.guile.sls")
;         '((rnrs) (rhs core) (prefix (guile) guile:))
;         '()
;         '())

(exit)
