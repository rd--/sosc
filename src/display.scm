; [byte] -> ()
(define osc-display
  (lambda (l)
    (zipWith
     (lambda (b n)
       (display (list (number->string b 16) (integer->char b)))
       (if (= 3 (mod n 4))
	   (newline)
	   (display #\space)))
     l
     (enumFromTo 0 (- (length l) 1)))))
