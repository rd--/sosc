; port -> int -> bytevector
(define read-bytevector
  (lambda (p n)
    (get-bytevector-n p n)))

; port -> int
(define read-i8
  (lambda (p)
    (decode-i8 (read-bytevector p 1))))

; port -> int
(define read-u8
  (lambda (p)
    (decode-u8 (read-bytevector p 1))))

; port -> int
(define read-i16
  (lambda (p)
    (decode-i16 (read-bytevector p 2))))

; port -> int
(define read-u16
  (lambda (p)
    (decode-u16 (read-bytevector p 2))))

; port -> int
(define read-i32
  (lambda (p)
    (decode-i32 (read-bytevector p 4))))

; port -> int
(define read-u32
  (lambda (p)
    (decode-u32 (read-bytevector p 4))))

; port -> int
(define read-i64
  (lambda (p)
    (decode-i64 (read-bytevector p 8))))

; port -> int
(define read-u64
  (lambda (p)
    (decode-u64 (read-bytevector p 8))))

; port -> double
(define read-f32
  (lambda (p)
    (decode-f32 (read-bytevector p 4))))

; port -> double
(define read-f64
  (lambda (p)
    (decode-f64 (read-bytevector p 8))))

; port -> string
(define read-pascal-string
  (lambda (p)
    (let* ((n (lookahead-u8 p))
	   (v (read-bytevector p (+ n 1))))
      (decode-pascal-string v))))

; port -> string
(define read-c-string
  (lambda (p)
    (let loop ((l nil)
	       (b (get-u8 p)))
      (if (= b 0)
	  (list->string (map integer->char (reverse l)))
	  (loop (cons b l) (get-u8 p))))))

; port -> string
(define read-osc-string
  (lambda (p)
    (let* ((s (read-c-string p))
	   (n (mod (c-string-length s) 4))
	   (i (- 4 (mod n 4))))
      (if (not (= n 0))
	  (read-bytevector p i)
	  #f)
      s)))

; port -> bytevector
(define read-blob
  (lambda (p)
    (let* ((n (read-i32 p))
	   (b (read-bytevector p n))
	   (i (- 4 (mod n 4))))
      (if (not (= n 0))
	  (read-bytevector p i)
	  #f)
      b)))
