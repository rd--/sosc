; bytevector -> int
(define decode-u8
  (lambda (v)
    (bytevector-u8-ref v 0)))

; bytevector -> int
(define decode-u16
  (lambda (v)
    (bytevector-u16-ref v 0 (endianness big))))

; bytevector -> int
(define decode-u32
  (lambda (v)
    (bytevector-u32-ref v 0 (endianness big))))

; bytevector -> int
(define decode-u64
  (lambda (v)
    (bytevector-u64-ref v 0 (endianness big))))

; bytevector -> int
(define decode-i8
  (lambda (v)
    (bytevector-s8-ref v 0)))

; bytevector -> int
(define decode-i16
  (lambda (v)
    (bytevector-s16-ref v 0 (endianness big))))

; bytevector -> int
(define decode-i32
  (lambda (v)
    (bytevector-s32-ref v 0 (endianness big))))

; bytevector -> int
(define decode-i64
  (lambda (v)
    (bytevector-s64-ref v 0 (endianness big))))

; bytevector -> double
(define decode-f32
  (lambda (v)
    (bytevector-ieee-single-ref v 0 (endianness big))))

; bytevector -> double
(define decode-f64
  (lambda (v)
    (bytevector-ieee-double-ref v 0 (endianness big))))

; bytevector -> string
(define decode-ascii-string
  (lambda (b)
    (utf8->string b)))

; bytevector -> string
(define decode-pascal-string
  (lambda (v)
    (let* ((n (decode-u8 v))
	   (w (bytevector-section v 1 (+ n 1))))
      (decode-ascii-string w))))

; bytevector -> string
(define decode-c-string
  (lambda (v)
    (let* ((n (bytevector-find-index v 0))
	   (w (bytevector-section v 0 n)))
      (decode-ascii-string w))))
