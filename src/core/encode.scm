; int -> bytevector
(define encode-u8
  (lambda (n)
    (bytevector-make-and-set-byte
     bytevector-u8-set!
     (exact n))))

; int -> bytevector
(define encode-u16
  (lambda (n)
    (bytevector-make-and-set
     bytevector-u16-set!
     2
     (exact n))))

; int -> bytevector
(define encode-u32
  (lambda (n)
    (bytevector-make-and-set
     bytevector-u32-set!
     4
     (exact n))))

; int -> bytevector
(define encode-u64
  (lambda (n)
    (bytevector-make-and-set
     bytevector-u64-set!
     8
     (exact n))))

; int -> bytevector
(define encode-i8
  (lambda (n)
    (bytevector-make-and-set-byte
     bytevector-s8-set!
     (exact n))))

; int -> bytevector
(define encode-i16
  (lambda (n)
    (bytevector-make-and-set
     bytevector-s16-set!
     2
     (exact n))))

; int -> bytevector
(define encode-i32
  (lambda (n)
    (bytevector-make-and-set
     bytevector-s32-set!
     4
     (exact n))))

; int -> bytevector
(define encode-i64
  (lambda (n)
    (bytevector-make-and-set
     bytevector-s64-set!
     8
     (exact n))))

; double -> bytevector
(define encode-f32
  (lambda (n)
    (bytevector-make-and-set
     bytevector-ieee-single-set!
     4
     (inexact n))))

; double -> bytevector
(define encode-f64
  (lambda (n)
    (bytevector-make-and-set
     bytevector-ieee-double-set!
     8
     (inexact n))))

; string -> bytevector
(define encode-ascii-string
  (lambda (s)
    (string->utf8 s)))

; string -> bytevector
(define encode-pascal-string
  (lambda (s)
    (let* ((b (encode-ascii-string s))
	   (n (encode-u8 (bytevector-length b))))
      (list n b))))

; string -> [bytevector]
(define encode-c-string
  (lambda (s)
    (let* ((b (encode-ascii-string s))
	   (z (encode-u8 0)))
      (list b z))))
