; (tcp, bundle) -> ()
(define tcpSendBundle
  (lambda (u o)
    (tcpSend u (encode-bundle o))))

; (tcp, message) -> ()
(define tcpSendMessage
  (lambda (u o)
    (tcpSend u (encode-message o))))

; (tcp, [message]) -> ()
(define tcpSendMessages
  (lambda (u m)
    (tcpSendBundle u (bundle -1 m))))

; (tcp, packet) -> ()
(define tcpSendPacket
  (lambda (u o)
    (tcpSend u (encode-packet o))))

; tcp -> maybe packet
(define tcpRecvPacket
  (lambda (u)
    (let ((b (tcpRecv u)))
      (if b (decode-packet b) #f))))
