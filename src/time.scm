; int
(define seconds-from-1900-to-1970
  (+ (* 70 365 24 60 60) (* 17 24 60 60)))

; float -> int
(define ntpr->ntp
  (lambda (n)
    (exact (round (* n (expt 2 32))))))

; float -> float
(define utc->ntpr
  (lambda (n)
    (+ n seconds-from-1900-to-1970)))

; int -> float
(define ntp->utc
  (lambda (n)
    (- (/ n (expt 2 32)) seconds-from-1900-to-1970)))
