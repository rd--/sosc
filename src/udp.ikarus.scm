; type udp = (input-port . output-port)

; string -> int -> udp
(define udpOpen
  (lambda (h p)
    (let-values
     (((i o) (ikarus:udp-connect h (number->string p))))
     (cons i o))))

; udp -> bytevector -> ()
(define udpSend
  (lambda (s b)
    (let ((o (cdr s)))
      (put-bytevector o b)
      (flush-output-port o))))

; udp -> maybe bytevector
(define udpRecv
  (lambda (s)
    (get-bytevector-some (car s))))

; udp -> ()
(define udpClose
  (lambda (s)
    (close-port (car s))
    (close-port (cdr s))))
