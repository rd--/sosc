(define-record-type udp* (fields s h p))

; string -> int -> udp
(define udpOpen
  (lambda (h p)
    (let ((s (guile:socket guile:PF_INET guile:SOCK_DGRAM 0)))
      (guile:connect s guile:AF_INET (guile:inet-pton guile:AF_INET h) p)
      (make-udp* s h p))))

; udp -> bytevector -> ()
(define udpSend
  (lambda (u b)
    (let ((s (udp*-s u)))
      (guile:send s b))))

; udp -> bytevector
(define udpRecv
  (lambda (u)
    (let ((b (make-bytevector (* 8192 4))))
      (guile:recv! (udp*-s u) b)
      b)))

;; udp -> ()
(define udpClose
  (lambda (u)
    (close-port (udp*-s u))))
