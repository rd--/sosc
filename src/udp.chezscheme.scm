; type udp = (input-port . output-port)

; string -> int -> udp
(define udpOpen
  (lambda (h p)
    (let ((cmd (string-append "nc -u " h " " (number->string p))))
      (let-values (((o i e pid) (chezscheme:open-process-ports cmd 'none #f)))
        (close-port e)
        (cons i o)))))

; udp -> bytevector -> ()
(define udpSend
  (lambda (s b)
    (let ((o (cdr s)))
      (put-bytevector o b)
      (flush-output-port o))))

; udp -> maybe bytevector
(define udpRecv
  (lambda (s)
    (get-bytevector-some (car s))))

; udp -> ()
(define udpClose
  (lambda (s)
    (close-port (car s))
    (close-port (cdr s))))
