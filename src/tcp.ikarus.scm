; type tcp = (input-port . output-port)

; string -> int -> tcp
(define tcpOpen
  (lambda (h p)
    (let-values
     (((i o) (ikarus:tcp-connect h (number->string p))))
     (cons i o))))

; tcp -> bytevector -> ()
(define tcpSend
  (lambda (s b)
    (let ((o (cdr s)))
      (put-bytevector o (encode-i32 (bytevector-length b)))
      (flush-output-port o)
      (put-bytevector o b)
      (flush-output-port o))))

; tcp -> maybe bytevector
(define tcpRecv
  (lambda (s)
    (let* ((i (car s))
	   (size (decode-i32 (get-bytevector-n i 4))))
      (get-bytevector-n i size))))

; tcp -> ()
(define tcpClose
  (lambda (s)
    (close-port (car s))
    (close-port (cdr s))))
