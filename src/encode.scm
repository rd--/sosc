; string -> int
(define c-string-length
  (lambda (s)
    (+ 1 (string-length s))))

; int -> int
(define osc-align
  (lambda (n)
    (- (fxand (+ n 3) (fxnot 3)) n)))

; int -> [bytevector]
(define padding-of
  (lambda (n) (replicate (osc-align n) (encode-u8 0))))

; string -> [bytevector]
(define encode-string
  (lambda (s)
    (list (encode-c-string s) (padding-of (c-string-length s)))))

; bytevector -> [bytevector]
(define encode-bytes
  (lambda (b)
    (let ((n (bytevector-length b)))
      (list (encode-i32 n)
	    b
            (padding-of n)))))

; datum -> [bytevector]
(define encode-value
  (lambda (e)
    (cond ((number? e) (if (integer? e)
			   (encode-i32 e)
			   (encode-f32 e)))
	  ((string? e) (encode-string e))
	  ((bytevector? e) (encode-bytes e))
	  (else (error "encode-value" "illegal value" e)))))

; [datum] -> [bytevector]
(define encode-types
  (lambda (l)
    (encode-string
     (list->string
      (cons #\,
	    (map (lambda (e)
		    (cond ((number? e) (if (integer? e) i32-tag f32-tag))
			  ((string? e) string-tag)
			  ((bytevector? e) blob-tag)
			  (else (error "encode-types" "type?" e))))
		 l))))))

; osc -> bytevector
(define encode-message
  (lambda (m)
    (flatten-bytevectors
     (list (encode-string (car m))
	   (encode-types (cdr m))
	   (map encode-value (cdr m))))))

; osc -> bytevector
(define encode-bundle-ntp
  (lambda (b)
    (flatten-bytevectors
     (list (encode-string "#bundle")
	   (encode-u64 (ntpr->ntp (car b)))
	   (map (lambda (e)
		  (if (message? e)
		      (encode-bytes (encode-packet e))
		      (error "encode-bundle" "illegal value" e)))
		(cdr b))))))

; osc -> bytevector
(define encode-bundle
  (lambda (b)
    (encode-bundle-ntp (cons (utc->ntpr (car b)) (cdr b)))))

; osc -> bytevector
(define encode-packet
  (lambda (p)
    (if (bundle? p)
	(encode-bundle p)
	(encode-message p))))

; camelCase
(define encodeMessage encode-message)
(define encodeBundle encode-bundle)
(define encodePacket encode-packet)
