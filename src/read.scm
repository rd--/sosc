; type datum = int32,i | int64,h | timetag,t | float,f | double,d | string,s | bytevector,b | midi,m

; char
(define i32-tag #\i)
(define i64-tag #\h)
(define u64-tag #\t)
(define f32-tag #\f)
(define f64-tag #\d)
(define string-tag #\s)
(define blob-tag #\b)
(define midi-tag #\m)

; port -> char -> datum
(define read-value
  (lambda (p t)
    (cond
     ((equal? t i32-tag) (read-i32 p))
     ((equal? t i64-tag) (read-i64 p))
     ((equal? t u64-tag) (read-u64 p))
     ((equal? t f32-tag) (read-f32 p))
     ((equal? t f64-tag) (read-f64 p))
     ((equal? t string-tag) (read-osc-string p))
     ((equal? t blob-tag) (read-blob p))
     ((equal? t midi-tag) (read-u32 p)) ; ?
     (else (error "read-value" "bad type" t)))))

; port -> [char] -> [datum]
(define read-arguments
  (lambda (p types)
    (if (null? types)
	'()
	(cons (read-value p (car types)) (read-arguments p (cdr types))))))

; port -> (string : [datum])
(define read-message
  (lambda (p)
    (let* ((address (read-osc-string p))
	   (types (read-osc-string p)))
      (cons address (read-arguments p (cdr (string->list types)))))))

; port -> (utc : [message])
(define read-bundle
  (lambda (p)
    (let ((bundle-tag (read-osc-string p))
	  (timetag (ntp->utc (read-u64 p)))
	  (parts (list)))
      (if (not (equal? bundle-tag "#bundle"))
	  (error "read-bundle"
		 "illegal bundle-tag"
		 bundle-tag)
	  (cons timetag
		(let loop ((parts (list)))
		  (if (eof-object? (lookahead-u8 p))
		      (reverse parts)
		      (begin
			; We have no use for the message size...
			(read-i32 p)
			(loop (cons (read-packet p) parts))))))))))

; byte
(define hash-u8 (char->integer #\#))

; port -> osc
(define read-packet
  (lambda (p)
    (if (equal? (lookahead-u8 p) hash-u8)
	(read-bundle p)
	(read-message p))))

; bytevector -> packet
(define decode-packet
  (lambda (b)
    (with-input-from-bytevector b read-packet)))
