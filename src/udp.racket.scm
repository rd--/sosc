(racket:define-struct udp* (s h p))

; string -> int -> udp
(define udp-open
  (lambda (h p)
    (make-udp* (racket:udp-open-socket h p) h p)))

; udp -> bytevector -> ()
(define udp-send
  (lambda (u b)
    (let ((s (udp*-s u))
	  (h (udp*-h u))
	  (p (udp*-p u)))
      (racket:udp-send-to* s h p b))))

; udp -> maybe bytevector
(define udp-recv
  (lambda (u)
    (let* ((s (udp*-s u))
	   (h (udp*-h u))
	   (p (udp*-p u))
	   (b (racket:make-bytes 8192))
	   (r (racket:sync/timeout 1.0 (racket:udp-receive!-evt s b))))
      (if r
	  (racket:subbytes b 0 (racket:car r))
	  #f))))

; udp -> ()
(define udp-close
  (lambda (u)
    (racket:udp-close (udp*-s u))))
