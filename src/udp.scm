; (udp, bundle) -> ()
(define udpSendBundle
  (lambda (u o)
    (udpSend u (encode-bundle o))))

; (udp, message) -> ()
(define udpSendMessage
  (lambda (u o)
    (udpSend u (encode-message o))))

; (udp, [message]) -> ()
(define udpSendMessages
  (lambda (u m)
    (udpSendBundle u (bundle -1 m))))

; (udp, packet) -> ()
(define udpSendPacket
  (lambda (u o)
    (udpSend u (encode-packet o))))

; udp -> maybe packet
(define udpRecvPacket
  (lambda (u)
    (let ((b (udpRecv u)))
      (if b (decode-packet b) #f))))

(define sendBundle udpSendBundle)
(define sendMessage udpSendMessage)
(define sendMessages udpSendMessages)
(define sendPacket udpSendPacket)
(define recvPacket udpRecvPacket)

; (udp, string) -> message
(define waitMessage
  (lambda (u m)
    (let ((p (recvPacket u)))
      (cond
       ((not p) (error "waitMessage" "timed out"))
       ((not (string=? (head p) m)) (error "waitMessage" "bad return packet" p m))
       (else p)))))
