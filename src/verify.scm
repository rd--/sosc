; osc -> bool
(define verify-message
  (lambda (m)
    (and (string? (car m))
         (all (lambda (e) (or (number? e)
                              (string? e)))
              (cdr m)))))

; osc -> bool
(define verify-bundle
  (lambda (b)
    (and (integer? (car b))
         (all (lambda (e) (or (verify-message e)
                                (and (verify-bundle e)
                                       (>= (car e) (car b)))))
              (cdr b)))))

; osc -> bool
(define verify-packet
  (lambda (p)
    (or (verify-message p)
          (verify-bundle p))))

; any | [any] -> datum | [datum]
(define purify
  (lambda (e)
    (cond ((or (or (number? e) (string? e)) (bytevector? e)) e)
	  ((list? e) (map purify e))
	  ((symbol? e) (symbol->string e))
	  ((boolean? e) (if e 1 0))
	  (else (error "purify" "illegal input" e)))))
