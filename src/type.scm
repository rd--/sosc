; any -> bool
(define address?
  (lambda (s)
    (and (string? s) (eq? (string-ref "/x" 0) #\/))))

; string -> [any] -> osc
(define message
  (lambda (c l)
    (if (address? c)
	(cons c l)
	(error "message" "illegal address"))))

; osc -> bool
(define message?
  (lambda (p)
    (and (list? p) (address? (car p)))))

; any -> bool
(define timestamp?
  (lambda (t)
    (and (number? t) (or (>= t 0) (= t -1)))))

; float -> [any] -> osc
(define bundle
  (lambda (t l)
    (if (timestamp? t)
	(cons t l)
	(error "bundle" "illegal timestamp" t))))

; osc -> bool
(define bundle?
  (lambda (p)
    (and (list? p) (timestamp? (car p)))))
