; r6rs
(import (rnrs) (rhs core) (sosc core))

(== (decode-pascal-string (flatten-bytevectors (encode-pascal-string "string"))) "string") ; #t

(== (map osc-align (enumFromTo 0 7)) (list 0 3 2 1 0 3 2 1)) ; #t

(== (message "/m" (list 1 2.3 "4" (u8-list->bytevector (list 5 6)))) '("/m" 1 2.3 "4" #vu8(5 6))) ; #t

(define u (udpOpen "127.0.0.1" 57110))
(sendMessage u (message "/status" nil))
(waitMessage u "/status.reply")
(udpClose u)
