sosc
----

[r6rs](http://r6rs.org/) scheme
[open sound control](http://opensoundcontrol.org/)

network communication is implemented for
[chezscheme](https://www.scheme.com/) &
[ikarus](https://launchpad.net/ikarus/) &
[guile](https://www.gnu.org/software/guile/)
schemes only.

tested with:
chezscheme-9.5.8,
ikarus-0.0.4-1870,
guile-3.0.4

© [rohan drape](http://rohandrape.net/) and others,
  1998-2024,
  [gpl](http://gnu.org/copyleft/).

see the revision
[history](?t=sosc&q=history)
for details
